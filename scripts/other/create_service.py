import os
import argparse

from jinja2 import Template

def parse_input_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--service", dest="service", help="Service name", required=True)
    parser.add_argument("--namespace", dest="namespace",choices=['default', 'demo', 'cloud-test'], help="Namespace", required=True)
    parser.add_argument("--environment", dest="environment", help="Environment", choices=['dev', 'production', 'qaintegration', "hotfix"], required=True)
    parser.add_argument("--image-tag", dest="image_tag", default='latest', help="Image Tag", required=False)
    parser.add_argument("--contains-services", dest="contains_services", default='false', help="Set to true if contains services. Specify whether NodePort or LoadBalancer by using flag", required=False)
    parser.add_argument("--node-port", dest="node_port", default='false', help="Set to true if service type is NodePort", required=False)
    parser.add_argument("--load-balancer", dest="load_balancer", default='false', help="Set to true if service type is LoadBalancer", required=False)
    parser.add_argument("--service-port", dest="service_port", default='false', help="Set to service port", required=False)
    parser.add_argument("--sa-file", dest="sa_file", help="Service account file", required=False)
    parser.add_argument("--cloud-armor", dest="cloud_armor", default='false', help="Set to true to use Cloud Armor policy", required=False)
    # service port
    # parser.add_argument("--var-names", dest="var_names", help="Custom variables. Define in format: Foo=foo BAR=bar", nargs='*', required=False)
    # parser.add_argument("--environments", dest="environments", const='dev production qaintegration hotfix', help="List of environments. Default: dev production qaintegration hotfix",
    #                     nargs='*', required=False)  # In future add multiple environment support                      
    args = parser.parse_args()
    return args

# This function opens given templates for reading
def render_input(file_name, **kwargs):
    with open('./templates/' + file_name) as my_file:
        template = Template(my_file.read())
        return template.render(**kwargs)

# This function opens given templates for writing
def write_to_file(file_name, file_content):
    with open('./' + file_name, mode='w') as output_file:
        output_file.write(file_content)

def create_kustomize_folders(environment, service):
    overlays_dir='overlays/' + environment
    base_dir='base/' + service
    service_dir='overlays/' + environment + '/' + service
    for folder in base_dir, overlays_dir, service_dir:
        try: 
            os.mkdir('outputs/' + folder)
        except OSError as err:
            print("OS error: {0}".format(err))
        # else:
        # os.mkdir(overlays_dir + '/user-data')
    return(base_dir , overlays_dir, service_dir)       

if __name__ == '__main__':
    input_args = parse_input_args()
    service = str(input_args.service)
    environment = str(input_args.environment)
    namespace = str(input_args.namespace)
    image_tag = str(input_args.image_tag)
    contains_services = str(input_args.contains_services)
    node_port = str(input_args.node_port)
    load_balancer = str(input_args.load_balancer)
    cloud_armor = str(input_args.cloud_armor)

    base_dir, overlays_dir, service_dir = create_kustomize_folders(environment, service)

    print('Base directory is: '+ base_dir + '\nOverlays directory is: ' + overlays_dir + '\nService diretory is: ' + service_dir)

    print('node port:' + node_port + ' contains services: ' + contains_services)
    
    backend_content = render_input(file_name='base/backend-template.j2',
                                    service=service,
                                    cloud_armor=cloud_armor)
    write_to_file(file_name='outputs/' + base_dir +'/' + service + '-backend.yaml', file_content=backend_content)    

    deployment_content = render_input(file_name='base/deployment-template.j2',
                                    image_tag=image_tag,
                                    service=service)
    write_to_file(file_name='outputs/' + base_dir +'/' + service + '-deployment.yaml', file_content=deployment_content)  

    hpa_content = render_input(file_name='base/hpa-template.j2',
                                    service=service)
    write_to_file(file_name='outputs/' + base_dir +'/' + service + '-hpa.yaml', file_content=hpa_content)  

    ingress_content = render_input(file_name='base/ingress-template.j2',
                                    service=service)
    write_to_file(file_name='outputs/' + base_dir +'/' + service + '-ingress.yaml', file_content=ingress_content)

    service_content = render_input(file_name='base/service-template.j2',
                                    service=service)
    write_to_file(file_name='outputs/' + base_dir +'/' + service + '-service.yaml', file_content=service_content)   

    kustomization_content = render_input(file_name='base/kustomization-template.j2',
                                          namespace=namespace,
                                          contains_services=contains_services,
                                          load_balancer=load_balancer,
                                          node_port=node_port,
                                          service=service)
    write_to_file(file_name='outputs/' + base_dir +'/kustomization.yaml', file_content=kustomization_content)         

    # for env in environment: # In future add multiple environment support

    overlays_deployment_content = render_input(file_name='overlays/deployment-template.j2',
                                                service=service)
    write_to_file(file_name='outputs/' + service_dir +'/' + service + '-deployment.yaml', file_content=overlays_deployment_content)
    
    overlays_kustomization_content = render_input(file_name='overlays/kustomization-template.j2',
                                                   namespace=namespace,
                                                   image_tag=image_tag,
                                                   service=service)
    write_to_file(file_name='outputs/' + service_dir +'/kustomization.yaml', file_content=overlays_kustomization_content)   

    if environment == 'production':
        overlays_hpa_content = render_input(file_name='overlays/hpa-template.j2',
                                            service=service)
        write_to_file(file_name='outputs/' + service_dir +'/' + service + '-hpa.yaml', file_content=overlays_hpa_content)  
