#!/bin/bash -e


function create_new_service() {
        images_identity_file=~/identities/storm-images.json
        environments=()
        secrets=()
        read -p 'Please enter the new service name: ' service

        echo 'Select the service Environments. Select "Finished" when done'
        select environment in 'Dev' 'Production' 'Hotfix' 'QAIntegration' 'Finished'
        do
                case $environment in
                'Dev'|'Production'|'Hotfix'|'QAIntegration'|'Finished')
                        [[ $environment = Finished ]] && break
                        environments+=(`echo $environment| tr '[:upper:]' '[:lower:]'| tr " " "-"`)
                        ;;
                *)
                        echo "Invalid selection"
                        ;;
                esac
        done

        echo 'Select the service Namespace'
        select namespace in 'Default' 'Demo' 'Cloud Test'
        do
                case $namespace in
                'Default'|'Demo'|'Cloud Test')
                        namespace=`echo $namespace| tr '[:upper:]' '[:lower:]'| tr " " "-"`
                        break
                        ;;
                *)
                        echo "Invalid selection"
                        ;;
                esac
        done

        echo 'Select the service type'
        select service_type in 'External' 'External with Cloud Armour' 'Internal Load Balancer' 'No service'
        do
                case $service_type in 
                'External'|'External with Cloud Armour'|'Internal Load Balancer'|'No service')
                        service_type=`echo $service_type| tr '[:upper:]' '[:lower:]'| tr " " "-"`
                        if [ $service_type != 'no-service' ]
                        then 
                                read -p 'Please enter '$service' services port: ' port
                        fi   
                        break
                        ;;
                *)
                        echo "Invalid selection" 
                        ;;
                esac
        done

        echo 'Select '$service's secrets. Select "Finished" when done'
        select secret in 'Service Account' 'Postgres' 'None' 'Finished'
        do
                case $secret in 
                'Service Account'|'Postgres'|'None'|'Finished')
                        [[ $secret = Finished ]] || [[ $secret = None ]] && break
                        secrets+=(`echo $secret| tr '[:upper:]' '[:lower:]'| tr " " "-"`)
                        ;;
                *)
                        echo "Invalid selection" 
                        ;;
                esac
        done

        echo $service
        echo $port
        echo ${environments[*]}
        echo $namespace
        echo $service_type
        echo ${secrets[*]}

        gcloud auth activate-service-account --project=storm-images --key-file=$images_identity_file
        image_tag=`gcloud container images list-tags eu.gcr.io/storm-images/storm-$service | head -2 | tail -1 | awk '{ print $2 }'`
        echo $image_tag
}

function create_new_environment() {
        echo create_new_environment
}
function add_configuration_to_service() {
        echo add_configuration_to_service
}

echo 'What would you like to do?'
select action in 'Create new service' 'Create new environment' 'Add configuration to service'
do
        case $action in
        'Create new service'|'Create new environment'|'Add configuration to service')
                action=`echo $action| tr '[:upper:]' '[:lower:]'| tr " " "_"`
                echo $action
                $action
                break
                ;;
        *)
                echo "Invalid selection"
                ;;
        esac
done
