import groovy.json.*
import java.util.*
import hudson.Util

def call(currentBuild) {
    def currentStage = ""
    try {
        node ('master'){
            stage ('Checkout SCM') {
                currentStage = 'Checkout SCM'
                deleteDir()
                checkout scm
            }
            stage ('Deploy Kubernetes'){
                
            }
        }
    } catch (e) {
        throw e
    }
}