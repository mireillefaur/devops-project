import groovy.json.*
import java.util.*
import hudson.Util

def call(currentBuild) {
    def currentStage = ""

    try {
        node ('master') {

            stage('Checkout SCM') {
                currentStage = 'Checkout SCM'
                deleteDir()
                checkout scm
            }
            stage('Build Image') {       
                currentStage = 'Build Image'              
                env.IMAGE_TAG = env.CI_TAG + '-' + env.BUILD_NUMBER
                dir( env.CI_PATH ) {                  
                    sh """
                    docker build -t "$CI_IMAGE:$IMAGE_TAG" .
                    """
                }
            }
            stage('Push Image to ECR') {
                env.CI_REGISTRY_IMAGE = env.CI_REGISTRY + '/' + env.CI_IMAGE
                currentStage = "Push Image to ECR"
                sh """
                $(aws ecr get-login --region ' + env.CI_REGION + ' --no-include-email)
                docker tag "$CI_IMAGE:$IMAGE_TAG" "$CI_REGISTRY_IMAGE:$IMAGE_TAG"
                docker push "$CI_REGISTRY_IMAGE:$IMAGE_TAG"
                docker rmi -f \$(docker images --filter "dangling=true" -q) || true
                """
            }

            stage('Tag Image as latest'){
                currentStage = 'Tag Image as latest'
                sh """
                docker tag "$CI_REGISTRY_IMAGE:$IMAGE_TAG" "$CI_REGISTRY_IMAGE:$env.CI_TAG-latest"
                docker push "$CI_REGISTRY_IMAGE:$env.CI_TAG-latest"
                """
            }
            // stage('Delete old images') {
            //     currentStage = 'Delete old images'
            //     // Keep the 3 last images per app
            //     sh """
            //         COUNT=`docker images | grep $CI_IMAGE | grep $CI_TAG | awk '{print \$3}' | uniq | wc -l`
            //         OLD=\$((\$COUNT - 3))
            //         if [ \$OLD -gt 0 ]
            //         then
            //             docker rmi -f `docker images |  grep $CI_IMAGE | grep $CI_TAG | awk '{print \$3}' | uniq | tail -\$OLD`
            //         fi
            //     """
            // }

        }
    } catch (e) {

        throw e
    }
}