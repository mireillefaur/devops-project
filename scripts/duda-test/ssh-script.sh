#!/bin/bash
source ~/conf

# constants
TIMEOUT_SECS=30

verify() {
	command="$1"
	while [ -z `eval $command` ]
	do
		if [ $SECONDS -gt $TIMEOUT_SECS ]
		then
			echo "Error! Operation timed out"
			exit 1
		else
			echo "Still waiting.."
			sleep 5
		fi	
	done
	echo "Done"
}

# remove from elb and wait for instance to be actually removed
remove_from_lb_and_wait() {
	echo "removing $1"
	
	#remove instance
    aws elbv2 deregister-targets --targets Id=$1 --target-group-arn $TARGET_GROUP_ARN
	
	#wait till actually removed

	command="aws elbv2 describe-target-health --targets Id=$1 --target-group-arn $TARGET_GROUP_ARN | jq '.TargetHealthDescriptions | .[] | .TargetHealth | .State' | grep  unused"
	verify "$command"

}

# insert into elb and wait for instance to be in service
insert_into_lb_and_wait() {
	echo "inserting $1"
	#add instance
    aws elbv2 register-targets --targets Id=$1 --target-group-arn $TARGET_GROUP_ARN
	
	command="aws elbv2 describe-target-health --targets Id=$1 --target-group-arn $TARGET_GROUP_ARN | jq '.TargetHealthDescriptions | .[] | .TargetHealth | .State' | grep  healthy"
	verify "$command"

}

# deploy
deploy_instance() {
	echo "deploying $1"
	# update ~/index.html with your name
	# to update use: sshInstance.sh INSTANCE_ID COMMAND
	./sshInstance.sh $1 "echo mireille > ~/index.html"
}

#main
main() {
	echo "ELB URL is: $ELB_URL"
	#get the list of instances from the LB
	list=`aws elbv2 describe-target-health --target-group-arn $TARGET_GROUP_ARN |jq '.TargetHealthDescriptions | .[] | .Target | .Id' | tr -d '"'`
	#loop over all instances in the LB (no need to change)
	for current in $list; do
		echo now at: $current
		remove_from_lb_and_wait $current
 		deploy_instance $current
		insert_into_lb_and_wait $current
	done
}

main