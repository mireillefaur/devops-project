#!/bin/bash
# This script will play movies on Shabbat at a chosen time.
# Note: set screen to not turn off for the amount of time needed. 
FOLDER='/mnt/c/Users/mirei/Desktop/Movies' # /Users/mfaur/Movies # Change if necessary
VLC_PATH='/mnt/c/Program\ Files/VideoLAN/VLC/vlc.exe' # vlc
ANOTHER='yes'
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NONE='\033[0m'
MOVIELIST='/tmp/movielist'
echo `date` > $MOVIELIST
echo "Time           Movie" > $MOVIELIST

IS_RUNNING=`ps -ef | grep /usr/sbin/atd | grep -v grep`
if [ -z "$IS_RUNNING" ]
then
    echo -e $GREEN"Starting the atd scheduler Service.."$NONE
    sudo service atd start
    echo -e $GREEN"Done!"$NONE      
fi    
while [ $ANOTHER != 'no' ] 
do
    COUNT=0
    FORMATS='mp4\|avi\|mkv\|mpeg\|mov\|flv' # Add more formats if necessary. also make grep include dot for percision
    ENABLESUBS='no'
    SUBFILE=''
    SUBFILECOUNT=0

    read -p "$(echo -e $BLUE"What is the movie name? (Must be located in $FOLDER): "$NONE)" MOVIE
    read -p "$(echo -e $BLUE"What time would you like the movie to run? Please enter in 24 hour format (e.g: 16:30): "$NONE)" TIME
    FINDNAME=$(echo $MOVIE | tr " " "*" )
    cd $FOLDER
    COUNT=`find . -xdev -iname "*$FINDNAME*" | grep $FORMATS | wc -l`
    while [ $COUNT != 1 ]
    do 
        if [ $COUNT -gt 1 ]
        then
            # Maybe give option to pick one of the files that were found like pick 1 or 2
            read -p "$(echo -e $RED"There is more then one movie file matching that name. Please specify the file type: "$NONE)" FORMATS
            COUNT=`find . -xdev -iname "*$FINDNAME*" | grep $FORMATS | wc -l`
            if [ $COUNT != 1 ]
            then 
                echo -e $RED"There is an error with the movie name. Please make sure movie name is unique and try again"$NONE
                exit 1
            fi
        elif [ $COUNT == 0 ]
        then
            echo -e $RED"No movie with that name was found. Please make sure movie is in the correct folder and try again"$NONE
            exit 1
        fi
    done
    FINALNAME=`find . -xdev -iname "*$FINDNAME*" | grep $FORMATS`
    read -p "$(echo -e $BLUE"Do you want to use subtitile file?  Please enter 'yes' or 'no': "$NONE)" ENABLESUBS
    if [ $ENABLESUBS == 'yes' ]
    then
        SUBFILECOUNT=`find . -xdev -iname "*$FINDNAME*" | grep "srt"|wc -l`
        if [ $SUBFILECOUNT -gt 1 ]
        then
            find . -xdev -iname "*$FINDNAME*" | grep ".srt"
            # Also here - give option to pick subtitle file without needing copy/paste (pick  1,2,3 etc)
            read -p "$(echo -e $BLUE"$SUBFILECOUNT subtitle files found. Please enter the file name that you want to use: "$NONE)" SUBFILE
        else #elif no sub file found with sub if - continue or not? 
            SUBFILE=`find . -xdev -iname "*$FINDNAME*" | grep "srt"`
        fi
        echo -e $GREEN
        echo ''${VLC_PATH}' "'${FINALNAME}'" --fullscreen --video-on-top --sub-file="'$SUBFILE'" --directx-volume=2' | at $TIME 2>&1 | fgrep -v 'warning: commands will be executed using /bin/sh'
    else
        echo ''${VLC_PATH}' "'${FINALNAME}'" --fullscreen --video-on-top --directx-volume=2' | at $TIME 2>&1 | fgrep -v 'warning: commands will be executed using /bin/sh'
        echo -e $NONE
    fi
    echo "$TIME          $MOVIE" >> $MOVIELIST
    # Test run movie to see that it works?
    read -p "$(echo -e $BLUE"Would you like to set up another movie? Please enter 'yes' or 'no': "$NONE)" ANOTHER
    if [ $ANOTHER == 'yes' ]
    then 
        echo -e $GREEN"Another! (in British accent)"$NONE
    else
        echo -e $GREEN"All set! to view and test jobs run: atq or: at -c <job_id>"$NONE
        echo -e $GREEN"Remember to set screen to not turn off and to set proper volume. Shabbat Shalom!"
        cat $MOVIELIST   
    fi   
done
# Add Cancelation/changing option
# Add option to put computer to sleep when done
# Maybe migrate to python?
