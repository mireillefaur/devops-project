terraform {
  required_version = ">= 0.11.14"

  backend "s3" {
    bucket = "mireille-devops-project"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.profile}"
}
