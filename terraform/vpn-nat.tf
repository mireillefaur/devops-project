# OpenVPN security group

resource "aws_security_group" "openvpn" {
  name   = "openvpn-sg"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "OpenVPN security group"

   tags = { 
       Name = "openvpn-sg"
       Terraform = "true" 
    }

  # For OpenVPN Client Web Server & Admin Web UI
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
    description = "OpenVPN UI"
  }

  ingress {
    protocol    = "udp"
    from_port   = 1194
    to_port     = 1194
    cidr_blocks = ["0.0.0.0/0"]
    description = "UDP client connection"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 943
    to_port     = 943
    cidr_blocks = ["79.183.18.30/32"]
    description = "Open VPN Admin UI"
  }
  # Make my IP a var

  ingress {
    protocol    = "tcp"
    from_port   = 945
    to_port     = 945
    cidr_blocks = ["0.0.0.0/0"]
    description = "TCP client connection"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["79.183.18.30/32"]
    description = "SSH access"
  }  
  # Make my IP a var
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# NAT instance secutiry group

resource "aws_security_group" "nat" {
  name   = "nat-sg"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "NAT security group"

   tags = { 
       Name = "nat-sg"
       Terraform = "true" 
    }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

} 

resource "aws_security_group_rule" "https-nat-to-nodes" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nat.id}"
  source_security_group_id = "${aws_security_group.from-nat-sg.id}"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  description              = "HTTPS access to nat"
}

resource "aws_security_group_rule" "http-nat-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nat.id}"
  source_security_group_id ="${aws_security_group.from-nat-sg.id}" 
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  description              = "HTTP access to nat"
}

resource "aws_security_group_rule" "ssh-nat-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nat.id}"
  source_security_group_id ="${aws_security_group.from-nat-sg.id}" 
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  description              = "SSH access to nat"
}


# Default security groups. Should be attached to all private instances.

# Allows private instances to be accessed from Openvpn.

resource "aws_security_group" "from-vpn-sg" {
  name        = "allow-access-from-vpn"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "allow-access-from-vpn"

  tags = {
    Name      = "allow-access-from-vpn"
    Terraform = "true"
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "https-node-to-vpn" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.from-vpn-sg.id}"
  source_security_group_id = "${aws_security_group.openvpn.id}"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  description              = "HTTPS access from vpn"
}

resource "aws_security_group_rule" "ssh-node-to-vpn" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.from-vpn-sg.id}"
  source_security_group_id = "${aws_security_group.openvpn.id}"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  description              = "SSH access from vpn"
}

resource "aws_security_group_rule" "http-node-to-vpn" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.from-vpn-sg.id}"
  source_security_group_id = "${aws_security_group.openvpn.id}"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  description              = "HTTP access from vpn"
}

# Allows private instances to be accessed from NAT Instance.

resource "aws_security_group" "from-nat-sg" {
  name        = "allow-access-from-nat"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "allow-access-from-nat"

  tags = {
    Name      = "allow-access-from-nat"
    Terraform = "true"
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "https-node-to-nat" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.from-nat-sg.id}"
  source_security_group_id = "${aws_security_group.nat.id}"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  description              = "HTTPS access from nat"
}

resource "aws_security_group_rule" "ssh-node-to-nat" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.from-nat-sg.id}"
  source_security_group_id = "${aws_security_group.nat.id}"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  description              = "SSH access from nat"
}

resource "aws_security_group_rule" "http-node-to-nat" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.from-nat-sg.id}"
  source_security_group_id = "${aws_security_group.nat.id}"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  description              = "HTTP access from nat"
}