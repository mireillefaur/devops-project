# data "aws_ami" "amazon-linux-1" {
#   most_recent = false

#   filter {
#     name   = "name"
#     values = ["amzn-ami-hvm-2018.03.0.20180508-x86_64-gp2"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
# }

resource "aws_security_group" "ec2" {
  name        = "allow-access-from-${var.env_name}-ec2-sg"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "ec2 security group"

  tags {
    Name = "allow-access-from-${var.env_name}-ec2-sg"
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Potential rule for future Kubernetes access

# resource "aws_security_group_rule" "ec2-access-kube" {
#   type                     = "ingress"
#   security_group_id        = "${aws_security_group.ec2.id}"
#   source_security_group_id = "${aws_security_group.from-vpn-sg.id}"
#   from_port                = 1111
#   to_port                  = 1111
#   protocol                 = "tcp"
#   description              = "ec2 Kubernetes access"
# }

resource "aws_instance" "ec2" {
  ami           = "ami-00068cd7555f543d5"
  instance_type = "t2.micro"
  key_name      = "${var.aws_ssh_key}"
  subnet_id     = "${element(aws_subnet.private.*.id, count.index)}"
  iam_instance_profile = "EC2AccessToECR"

  vpc_security_group_ids = [
    "${aws_security_group.from-vpn-sg.id}",
    "${aws_security_group.from-nat-sg.id}",
    "${aws_security_group.ec2.id}",
  ]

  availability_zone = "${var.aws_region}${element(var.availability_zones, count.index)}"

  tags {
    Name       = "ec2-${element(var.instance_number, count.index)}.${var.env_name}.${var.dns_zone_name}"
    Terraform  = "true"
  }

  count = "${var.ec2_count}"
}

resource "aws_instance" "nat" {
  ami               = "ami-00a9d4a05375b2763"
  instance_type     = "t2.micro"
  key_name          = "${var.aws_ssh_key}"
  subnet_id         = "${element(aws_subnet.public.*.id, count.index)}"
  source_dest_check = "false"
  associate_public_ip_address = "true"

  vpc_security_group_ids = [
    "${aws_security_group.nat.id}"
  ]

  availability_zone = "${var.aws_region}${element(var.availability_zones, count.index)}"

  tags {
    Name       = "nat-instance.${var.dns_zone_name}"
    Terraform  = "true"
  }

#   count = "${var.ec2_count}"
}

resource "aws_eip" "nat-ip" {
  instance = "${aws_instance.nat.id}"
  tags { Name = "nat-eip"}
  vpc      = true
}

resource "aws_instance" "openvpn" {
  ami           = "ami-0ca1c6f31c3fb1708"
  instance_type = "t2.micro"
  key_name      = "${var.aws_ssh_key}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
  associate_public_ip_address = "true"

  vpc_security_group_ids = [
    "${aws_security_group.openvpn.id}"
  ]

  availability_zone = "${var.aws_region}${element(var.availability_zones, count.index)}"

  tags {
    Name       = "openvpn-instance.${var.dns_zone_name}"
    Terraform  = "true"
  }

#   count = "${var.ec2_count}"
}

resource "aws_eip" "openvpn-ip" {
  instance = "${aws_instance.openvpn.id}"
  tags { Name = "openvpn-eip"}
  vpc      = true
}
