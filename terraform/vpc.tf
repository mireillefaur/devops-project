resource "aws_vpc" "vpc" {
  cidr_block           = "${var.vpc_cidr_block}"
  enable_dns_hostnames = true
  enable_dns_support   = true
  instance_tenancy     = "default"

  tags = "${map("Name", "${var.env_name}", "env_name", "${var.env_name}", "Terraform", "true")}"
}

resource "aws_subnet" "private" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "${cidrsubnet(aws_vpc.vpc.cidr_block, 3, 1 + count.index)}"

  availability_zone = "${var.aws_region}${element(var.availability_zones, count.index)}"

  tags = "${map("Name", "private-${var.aws_region}${element(var.availability_zones, count.index)}.${var.env_name}", "env_name", "${var.env_name}", "Terraform", "true", "SubnetType", "Private")}"

  count = "${var.availability_zone_count}"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    env_name  = "${var.env_name}"
    Name      = "private-${var.aws_region}${element(var.availability_zones, count.index)}.${var.env_name}"
    Terraform = "true"
  }

  count = "${var.private_rtb_count}"
}

resource "aws_route_table_association" "private" {
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"

  count = "${var.availability_zone_count}"
}

resource "aws_route" "private-subnet-to-nat" {
  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  instance_id         = "${element(aws_instance.nat.*.id, count.index)}"

  count = "${var.private_rtb_count}"
}

resource "aws_subnet" "public" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "${cidrsubnet(aws_vpc.vpc.cidr_block, 6, count.index)}"

  availability_zone = "${var.aws_region}${element(var.availability_zones, count.index)}"

  tags = "${map("Name", "public-${var.aws_region}${element(var.availability_zones, count.index)}.${var.env_name}", "env_name", "${var.env_name}", "Terraform", "true", "SubnetType", "Public")}"

  count = "${var.availability_zone_count}"
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.vpc.id}"
  count = "${var.public_rtb_count}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    env_name  = "${var.env_name}"
    Name      = "public-${var.aws_region}${element(var.availability_zones, count.index)}.${var.env_name}"
    Terraform = "true"
  }

  count = "${var.public_rtb_count}"
}

resource "aws_route_table_association" "public" {
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.public.*.id, count.index)}"

  count = "${var.availability_zone_count}"
}

resource "aws_route" "public-subnet-to-igw" {
  route_table_id         = "${element(aws_route_table.public.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
  count = "${var.public_rtb_count}"
}

resource "aws_network_acl" "network" {
  vpc_id = "${aws_vpc.vpc.id}"

  subnet_ids = [
    "${aws_subnet.private.*.id}",
    "${aws_subnet.public.*.id}",
  ]

  ingress {
    from_port  = 0
    to_port    = 0
    rule_no    = 100
    action     = "allow"
    protocol   = "-1"
    cidr_block = "0.0.0.0/0"
  }

  egress {
    from_port  = 0
    to_port    = 0
    rule_no    = 100
    action     = "allow"
    protocol   = "-1"
    cidr_block = "0.0.0.0/0"
  }

  tags = {
    Name      = "${var.env_name}"
    Terraform = "true"
  }
}