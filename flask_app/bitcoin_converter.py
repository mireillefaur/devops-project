from forex_python.bitcoin import BtcConverter
from flask import Flask

app = Flask(__name__)

@app.route("/BitCoin/<currency>")
def run_converter(currency):
    if currency != 'EUR' and currency != 'USD':
        return "Bad Request", 500 
    else:     
        rate_result = converter(currency)
        return str(rate_result)

def converter(currency):
    rate = BtcConverter()   
    rate_result = rate.get_latest_price(currency)
    print(rate_result)
    return(rate_result)

if __name__ == '__main__':
    app.run()