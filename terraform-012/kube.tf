#
# security group(s) for access to Load balancers for kubernetes cluster
#

# If I want to accees cluster publicly
resource "aws_security_group" "allow-internet-access-to-kube-cluster" {
  name        = "allow-internet-access-to-kube-cluster"
  vpc_id      = aws_vpc.vpc.id
  description = "allow-internet-access-to-kube-cluster"

  tags = {
    Name      = "allow-internet-access-to-kube-cluster"
    Terraform = "true"
    env_name  = var.env_name
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow HTTP traffic"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow HTTPS traffic"
  }
}

resource "aws_security_group_rule" "https-kube-to-vpn" {
  type              = "ingress"
  security_group_id = aws_security_group.from-vpn-sg.id
  cidr_blocks       = ["10.176.0.0/16"] # Change to SG?
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  description       = "kube access test"
}

resource "aws_security_group_rule" "http-kube-to-vpn" {
  type              = "ingress"
  security_group_id = aws_security_group.from-vpn-sg.id
  cidr_blocks       = ["10.176.0.0/16"] # Change to SG?
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  description       = "kube access test"
}

resource "aws_security_group_rule" "ssh-kube-to-vpn" {
  type              = "ingress"
  security_group_id = aws_security_group.from-vpn-sg.id
  cidr_blocks       = ["10.176.0.0/16"] # Change to SG?
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  description       = "kube access test"
}

