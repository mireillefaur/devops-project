variable "profile" {
  default = "default"
}

variable "aws_region" {
  default = "us-east-1"
}

variable "instance_number" {
  type    = list(string)
  default = ["1", "2", "3"]
}

variable "dns_zone_name" {
  default = "internal.devops"
}

variable "env_name" {
  default = "devops-project"
}

variable "ec2_count" {
  default = 1
}

variable "aws_ssh_key" {
  default = "devops-project-key"
}

variable "backet_name" {
  default = "mireille-devops-project"
}

variable "vpc_cidr_block" {
  default = "10.176.0.0/16"
}

variable "availability_zones" {
  type    = list(string)
  default = ["a", "b", "c"]
}

variable "availability_zone_count" {
  default = 2
}

variable "private_rtb_count" {
  default = 1
}

variable "public_rtb_count" {
  default = 1
}

